function handle = plot_history( history, handle )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

if (nargin < 2)
    xset = [];
    yset = [];
    for state = history
        xset = [xset state.x];
        yset = [yset state.y];
    end
    handle = plot(xset, yset, 'g');
    axis equal;
else
    xset = [];
    yset = [];
    for state = history
        xset = [xset state.x];
        yset = [yset state.y];
    end
    set(handle, 'XData', xset, 'YData', yset); 
end

end


