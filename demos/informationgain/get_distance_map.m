function [ distance_map ] = get_distance_map( grid_map_struct )
%GET_DISTANCE_MAP Summary of this function goes here
%   Detailed explanation goes here
distance_map = grid_map_struct;
distance_map.data(distance_map.data<1)=0;
distance_map.data(distance_map.data>1)=1;
distance_map.data = bwdist(distance_map.data);
end

