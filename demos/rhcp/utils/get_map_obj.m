function [ world_map, robo_map ] = get_map_obj( filename, grid_params, state, bounding_box )
%GET_MAP_OBJ Summary of this function goes here
%   Detailed explanation goes here

map = getMapfromImage(filename);

world_map.data = map;
world_map.scale = 1;
world_map.min = [1,1];

if (nargin > 2)
    robo_map.data = expose_map_belief( repmat([state.x state.y], [2 1]) + bounding_box, world_map.data, robo_map.data );
end

robo_map.data = grid_params.log_odds_occ*ones(size(map));  
robo_map.scale = world_map.scale;
robo_map.min = world_map.min;
end

