function reached = has_reached_goal( state, problem )
%HAS_REACHED_GOAL Summary of this function goes here
%   Detailed explanation goes here
reached = norm([state.x state.y] - [problem.goal.x problem.goal.y]) < problem.goal_tol;
end

