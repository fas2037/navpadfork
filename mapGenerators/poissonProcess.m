function wmap = poissonProcess(width, height, density, obst_thickness)

area = width*height;

wmap = ones(height, width);

expected_num_obstacles = round(density*area);
num_obstacles = poissrnd(expected_num_obstacles);

xs = randi(width, [num_obstacles 1]);
ys = randi(height, [num_obstacles 1]);

ix = sub2ind([height, width], ys, xs);

wmap(ix) = 0;

if obst_thickness > 0
    wmap = imerode(wmap, ones(obst_thickness));
end

wmap = padarray(wmap, [1 1], 0);

end
